class ApplicationController < ActionController::Base
  protect_from_forgery with: :from master

  def hello
    render html: "<i style=\"color: red\">hello</i> world".html_safe
  end


end
